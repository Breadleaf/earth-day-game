#!/usr/bin/python

import os
import time


def credits_scene():
    os.system('cls' if os.name == 'nt' else 'clear')
    print("  -------  Earth Day Game  -------  ")
    time.sleep(2)
    print("Programming team:  Bradley Hutchings")
    time.sleep(2)
    print("Story design team: Bradley Hutchings")
    time.sleep(2)
    print("Artwork:           Bradley Hutchings")
    time.sleep(2)
    print("I think:           You get the point", "", sep="\n")
    time.sleep(2)
    print("Please consider checking out my git-",
          "lab at gitlab.com/Breadleaf or my   ",
          "github at github.com/Breadleaf.     ",
          "",
          "I hope you learned something from   ",
          "this game. Please remember that     ",
          "there is always a way you can help  ",
          "the Earth by simply being a little  ",
          "more thoughtful about your every-   ",
          "day life.", "", sep="\n")


def grade(array, number):
    os.system('cls' if os.name == 'nt' else 'clear')
    total_score = 0
    for item in array:
        total_score += item
    final_score = (total_score / number) * 100
    print("You scored {}% out of {} questions.".format(final_score, number),
          "{}".format(
              "You did well!" if final_score >= 51 else "You have some room for improvement, but don't give up!"),
          "", sep="\n")


def repeat_string(sentence, repeats, dots):
    for a in range(repeats):
        for i in range(dots):
            os.system('cls' if os.name == 'nt' else 'clear')
            j = i + 1
            print(sentence, "." * j)
            time.sleep(.5)


def loading_screen(x, y):
    repeat_string("loading game", 3, 3)
    repeat_string("checking terminal size", 2, 3)
    if x < 50 or y < 35:
        print("Please make your window bigger or full screen")
        input("Press enter once the window is full screen.")


def info_page():
    print("+-------- How to play --------+",
          "| In this game you will go    |",
          "| through a series of events  |",
          "| in which you will be asked  |",
          "| to make an decision based   |",
          "| on the current situation.   |",
          "| Your goal is to reduce your |",
          "| environmental footprint as  |",
          "| much as you can.            |",
          "+-----------------------------+",
          "", sep="\n")


def tutorial_level():
    os.system('cls' if os.name == 'nt' else 'clear')
    score = 0
    while True:
        print("****************** Note *********************",
              "One thing to note is that in the actual game,",
              "there are scenes to go along with the question.",
              "", sep="\n")
        print("Scenario: You are at the grocery store you",
              "have the choice of either paper or plastic",
              "bags for your items. Which do you choose?", sep="\n")

        user_input = str(input("[Paper/Plastic]: ").strip().lower())
        if user_input == "paper":
            score = 1
            break
        elif user_input == "plastic":
            score = 0
            break
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("Please enter a valid input.", "", sep="\n")
    print()
    return score


def tutorial():
    score = []
    questions_asked = 0
    need_tutorial = True
    while need_tutorial:
        print("Would you like to play the tutorial level?")
        user_input = str(input("[Yes/No]: ").strip().lower())
        if user_input == "yes" or user_input == "y":
            score.append(tutorial_level())
            questions_asked += 1
            grade(score, questions_asked)
            input("Press enter when you are ready to move on.")
            break
        elif user_input == "no" or user_input == "n":
            need_tutorial = False
            break
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("Please enter a valid input.", "", sep="\n")
    return need_tutorial


def city_scene():
    os.system('cls' if os.name == 'nt' else 'clear')
    score = 0
    while True:
        city = open("city.txt", "rt")
        city_park = open("city_park.txt", "rt")
        city_mall = open("city_mall.txt", "rt")
        for line in city:
            print(line, end="")
        print()
        print("Scenario: You are the mayor of a small town. You",
              "have been given the choice of deciding between  ",
              "two construction projects. Your choice is to fund",
              "either a mall, a park, or do nothing at all.", sep="\n")
        user_input = str(input("[Mall/Park/Nothing]: ").strip().lower())
        os.system('cls' if os.name == 'nt' else 'clear')
        if user_input == "mall":
            for line in city_mall:
                print(line, end="")
            print()
            print("You choose the mall. This option was a  ",
                  "popular decision among the towns people.",
                  "However shortly there after, an issue   ",
                  "occurs, the mall has put the local shop ",
                  "out of business, as well as needing to  ",
                  "pave new roads for the influx of traffic.",
                  "This has been pretty bad for the cities ",
                  "local wild life", sep="\n")
            score = 0
            break
        elif user_input == "park":
            for line in city_park:
                print(line, end="")
            print()
            print("You chose the park. This decision is pretty",
                  "solid. Local children become more active and",
                  "can often be seen outside playing together  ",
                  "and eating apples off of the apple trees.   ",
                  "The park wasn't too bad for the environment  ",
                  "as the field was already empty.", sep="\n")
            score = 1
            break
        elif user_input == "nothing":
            city = open("city.txt", "rt")
            for line in city:
                print(line, end="")
            print()
            print("You choose to do nothing. While this is a",
                  "decent choice, the addition of the park  ",
                  "could have brought shelter for local wild-",
                  "life and a good spot for a future commun-",
                  "ity garden.", sep="\n")
            score = 0.5
            break
        else:
            print("Please enter a valid input.", "", sep="\n")
    print()
    input("Press enter when you are ready to move on.")
    return score


def soda_scene():
    os.system('cls' if os.name == 'nt' else 'clear')
    score = 0
    while True:
        soda = open("bottle.txt", "rt")
        for line in soda:
            print(line, end="")
        print()
        print("Scenario: You have just finished a cola. Your",
              "task is to decide if you want to put the bottle",
              "in the trash, recycling, or reuse the bottle.", sep="\n")
        user_input = str(input("[Trash/Recycle/Reuse]: ").strip().lower())
        os.system('cls' if os.name == 'nt' else 'clear')
        if user_input == "trash":
            soda_trash = open("bottle_trash.txt", "rt")
            for line in soda_trash:
                print(line, end="")
            print()
            print("You choose to put the bottle in the trash. This",
                  "option will send the bottle straight to a land-",
                  "fill sight where the bottle will decay for     ",
                  "decades, never properly composting. Eventually ",
                  "it will break apart into small pieces and cause",
                  "varying levels of damage to the environment.", sep="\n")
            score = 0
            break
        elif user_input == "recycle":
            soda_recycle = open("bottle_recycle.txt", "rt")
            for line in soda_recycle:
                print(line, end="")
            print()
            print("You choose to put the bottle into the recycle",
                  "bin. This option is better than putting the  ",
                  "bottle in the trash, however it is not a     ",
                  "guarantee that the bottle will make it to a  ",
                  "place that can properly recycle it. Many     ",
                  "things put in the recycle bin never make it  ",
                  "to a recycling center. This is why many people",
                  "like to bring their bottles to collection    ",
                  "sites. These collection sites will also give ",
                  "you a small amount of money (depending on your",
                  "state/country's rules). In the US this is     ",
                  "Called CRV.", sep="\n")
            score = 0.5
            break
        elif user_input == "reuse":
            soda_reuse = open("bottle_reuse.txt", "rt")
            for line in soda_reuse:
                print(line, end="")
            print()
            print("You choose to reuse the bottle to make a flower",
                  "pot. Not only does this help the environment, it",
                  "also is a cheap way of adding a bit of beauty to",
                  "a room in a home, or office space.", sep="\n")
            score = 1
            break
        else:
            print("Please enter a valid input.", "", sep="\n")
    print()
    input("Press enter when you are ready to move on.")
    return score


def store_scene():
    os.system('cls' if os.name == 'nt' else 'clear')
    score = 0
    while True:
        store = open("clothing.txt", "rt")
        for line in store:
            print(line, end="")
        print()
        print("You are in the market for some new",
              "cloths. There are two stores in   ",
              "your town that you can think of that",
              "would carry cloths. Your job is to",
              "pick the most environmentally     ",
              "sustainable option", sep="\n")
        user_input = str(input("[Thrift/Chain]: ").strip().lower())
        os.system('cls' if os.name == 'nt' else 'clear')
        if user_input == "thrift":
            store_thrift = open("clothing_thrift.txt", "rt")
            for line in store_thrift:
                print(line, end="")
            print()
            print("You choose to shop at the thrift store.",
                  "This option is both affordable and better",
                  "for the environment, as it supports reuse",
                  "of old goods.", sep="\n")
            score = 1
            break
        elif user_input == "chain":
            store_chain = open("clothing_target.txt", "rt")
            for line in store_chain:
                print(line, end="")
            print()
            print("You choose to shop at the chain store.",
                  "While this isn't inherently bad, it is",
                  "nice to support your local business when",
                  "you can. One thing to note is that you ",
                  "cannot always verify where the product ",
                  "you purchase is coming from when you buy",
                  "from large chain stores.", sep="\n")
            score = 0.5
            break
        else:
            print("Please enter a valid input.", "", sep="\n")
    print()
    input("Press enter when you are ready to move on.")
    return score


def event_cycle():
    scores = []
    questions_asked = 0
    scores.append(city_scene())
    questions_asked += 1
    scores.append(soda_scene())
    questions_asked += 1
    scores.append(store_scene())
    questions_asked += 1
    os.system('cls' if os.name == 'nt' else 'clear')
    return scores, questions_asked


def play_again_screen():
    while True:
        print("Would you like to play again?")
        user_input = str(input("[Yes/No]: ").strip().lower())
        if user_input == "yes" or user_input == "y":
            game()
            break
        elif user_input == "no" or user_input == "n":
            credits_scene()
            break
        else:
            print("Please enter a valid input.")


def game():
    os.system('cls' if os.name == 'nt' else 'clear')
    term_x = os.get_terminal_size()[0]
    term_y = os.get_terminal_size()[1]
    loading_screen(term_x, term_y)
    os.system('cls' if os.name == 'nt' else 'clear')
    info_page()
    input("When you are ready to begin, press enter")
    os.system('cls' if os.name == 'nt' else 'clear')
    tutorial()
    scores, questions_asked = event_cycle()
    grade(scores, questions_asked)
    play_again_screen()


def debug():
    term_x = os.get_terminal_size()[0]
    term_y = os.get_terminal_size()[1]
    if term_x < 50 or term_y < 35:
        print("Please make your window bigger or full screen")
        input("Press enter once the window is full screen.")
    tutorial()
    scores, questions_asked = event_cycle()
    grade(scores, questions_asked)
    play_again_screen()


if __name__ == "__main__":
    # debug()
    game()
    # print("If you see this, please uncomment \"game()\" in the file \"game.py\".",
    #       "In the event you don't know how to edit this file, go ahead and",
    #       "press the \"Enter\" key on your keyboard to continue to the game.", sep="\n")
    # mode = str(input("Enter Mode: "))
    # debug() if mode == "d" else game()
